def kmp_table(W):
    T = [-1]*len(W)
    pos = 1
    cnd = 0
    # T[0] = -1
    while pos < len(W):
        if W[pos] == W[cnd]:
            T[pos] = T[cnd]
        else:
            T[pos] = cnd
            while cnd >= 0 and W[pos] != W[cnd]:
                cnd = T[cnd]
        pos = pos + 1
        cnd = cnd + 1

    # T[pos] = cnd # (only needed when all word occurrences are searched)
    return T

def kmp_search(pat, txt): 
    M = len(pat)
    N = len(txt) 
    j = 0
    lps = kmp_table(pat)
    print('lps=', lps)
    i = 0 
    while i < N: 
        if pat[j] == txt[i]: 
            i += 1
            j += 1
  
        if j == M: 
            return (i-j) 
            j = lps[j-1] 
  
        elif i < N and pat[j] != txt[i]: 
            if j != 0: 
                j = lps[j-1] 
            else: 
                i += 1

print('kmp_search("ABABCABAB","ABABDABACDABABCABAB")=', kmp_search("ABABCABAB","ABABDABACDABABCABAB"))
